package xmlReader;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

class Person {
    private String name;
    private String surname;
    private String age;
    private String city;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}


public class ReadXMLFiles {

    public static void main(String[] args) {


        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                boolean bfname = false;
                boolean blname = false;
                boolean bage = false;
                boolean bcity = false;
                Person person = null;
                List<Person> personList = new ArrayList<Person>();

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

                    if (qName.equals("person")) {
                        person = new Person();
                    }
                    if (qName.equals("name")) {
                        bfname = true;
                    } else if (qName.equals("surname")) {
                        blname = true;
                    } else if (qName.equals("age")) {
                        bage = true;
                    } else if (qName.equals("city")) {
                        bcity = true;
                    }
                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {

                    if (qName.equals("person")) {
                        personList.add(person);
                        person = null;
                    }
                    if (qName.equals("name")) {
                        bfname = false;
                    } else if (qName.equals("surname")) {
                        blname = false;
                    } else if (qName.equals("age")) {
                        bage = false;
                    } else if (qName.equals("city")) {
                        bcity = false;
                    }
                }

                @Override
                public void characters(char[] chars, int start, int length) throws SAXException {

                    if (bfname) {
                        person.setName(new String(chars, start, length));
                    } else if (blname) {
                        person.setSurname(new String(chars, start, length));
                    } else if (bage) {
                        person.setAge(new String(chars, start, length));
                    } else if (bcity) {
                        person.setCity(new String(chars, start, length));
                    }

                }

                @Override
                public void endDocument() throws SAXException {

                    try {
                        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "Adrian", "myszka777");
                        System.out.println("Database connection succesful!\n");

                        PreparedStatement preparedStatement;

                        for (Person person : personList) {
                            preparedStatement = connection.prepareStatement("INSERT INTO xml(name,surname,age,city) VALUE(?,?,?,?)");
                            preparedStatement.setString(1, person.getName());
                            preparedStatement.setString(2, person.getSurname());
                            preparedStatement.setString(3, person.getAge());
                            preparedStatement.setString(4, person.getCity());
                            preparedStatement.executeUpdate();
                        }


                    } catch (SQLException e) {
                        e.printStackTrace();

                    }
                }

            };

            saxParser.parse("dane-osoby.xml", handler);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
